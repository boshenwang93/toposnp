#! /usr/bin/python3

import os
import re

#### Make BLAST DATABASE CMD
#### makeblastdb -in allPDB.fasta -dbtype prot -out pdb

#### BLAST Search options
#### E-value < 1E-6,  Max hits < 100, outformat XML
## blastp -db pdb -query XXXX.fasta -out XXXX.xml
## -num_threads 10 -evalue 1E-6 -max_hsps 100 -matrix BLOSUM62 -outfmt 5

def RunBLAST(query_fasta_path, output_XML_folder, BLAST_DB_name):
    ## output xml file name 
    fasta_name = re.split('/', query_fasta_path)[-1]
    uniprot_id = re.split('.fasta', fasta_name)[0]
    output_XML_path = output_XML_folder + uniprot_id + '.XML'

    ## cmd 
    blast_cmd = 'blastp -db ' + BLAST_DB_name +\
                ' -query '+ query_fasta_path +\
                ' -out ' + output_XML_path +\
                ' -num_threads 10 -evalue 1E-6 -max_hsps 1000 -matrix BLOSUM62' +\
                ' -outfmt 5'
                
    os.system(blast_cmd)

    return 0


def main():

    ## fasta path 
    fasta_path = '/home/bwang/project/toposnp/data/fasta/'

    ## output path 
    output_path = '/home/bwang/project/toposnp/data/blast/out_xml/'

    ## BLAST all fasta in folder 
    for root, subdirs, files in os.walk(fasta_path):
        for single_fasta_file in files:
            fasta_abs_path = root + single_fasta_file

            if single_fasta_file.startswith('NP'):
                RunBLAST(query_fasta_path = fasta_abs_path,
                         output_XML_folder = output_path,
                         BLAST_DB_name = 'pdb')
    return 0 

if __name__ == '__main__':
    main()