#! /usr/bin/python3 

import re 

#### For the fasta file containing multiple entries 
#### Seperate each single one to a single file named as Uniprot Name 

def SeperateFasta(Combinated_Fasta_File, out_fasta_dir):

    ## read whole content into a string variable 
    with open(Combinated_Fasta_File) as f:
        all_context = f.read()
    f.close()
    
    ## split each sequence context 
    list_slices = re.split('>', all_context)
    
    ## iterate each sequence 
    for each_sequence in list_slices:
        sequence_with_header = '>' + each_sequence
        
        list_lines_seq = re.split("\n", sequence_with_header)

        ## to obtain header and actual sequence(Remove \n)
        uniprot_header = ''
        seq_context = ''

        for eachline in list_lines_seq:
            if eachline.startswith('>') == 0 :
                if eachline != '':
                    seq_context += eachline
            else:
                uniprot_header += eachline

        ## get uniprot id 
        try:
            uniprot_id = re.split("\|", uniprot_header)[1].strip().upper()

            ## new file name & path 
            new_file_name = uniprot_id + '.fasta'
            new_file_path = out_fasta_dir + new_file_name

            # file content 
            new_content = '>' + uniprot_id + "\n" + seq_context
            
            # write to new file 
            out = open(new_file_path, 'w')
            out.write(new_content)
            out.close()
        except:
            print(uniprot_header)
    
    return 0

if __name__ == '__main__':
    SeperateFasta(Combinated_Fasta_File =\
                    '/data/Dropbox/project/cancerSNP/data/BenchDataset/Polyphen2Origin/human-2011_12.seq',
                  out_fasta_dir = '/data/Dropbox/project/cancerSNP/data/BenchDataset/Polyphen2Origin/fasta/')