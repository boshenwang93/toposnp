#! /usr/bin/python3

import re 
import os 
from multiprocessing import Process, Pool


#### Run clustalW for pairwise align  
def RunClustw(pairname):

    input_fasta = '/data/database/cancerSNP/benchmark/humvar/Pairwise/raw/' + pairname
    output_fasta = '/data/database/cancerSNP/benchmark/humvar/Pairwise/aligned/' + pairname

    cmd = 'clustalw -TYPE=PROTEIN -OUTPUT=FASTA -PWMATRIX=BLOSUM ' +\
          ' -INFILE=' + input_fasta +\
          ' -OUTFILE=' + output_fasta 
    os.system(cmd)

    return 0


def main():
    list_file_name = []
    
    for root, subdir_list, file_list in os.walk('/data/database/cancerSNP/benchmark/humvar/Pairwise/raw/'):
        for each_file in file_list:
            if each_file.endswith('.fasta'):
                list_file_name.append(each_file)

    with Pool(processes= 10 ) as pool:
        print(pool.map(RunClustw, list_file_name))

    return 0 


if __name__ == '__main__':
    main()