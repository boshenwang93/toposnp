#! /usr/bin/python3

import re
import os 

###########################################################
#### Calculate two-way identity  ##########################
###########################################################

def ReadIdentity(alignedFasta_file):
    uniprotID1 =''
    uniprotID2 = ''

    with open(alignedFasta_file) as f:
        all_context = f.read()
    f.close()

    list_slices = re.split('>', all_context)

    seq1_w_header = '>' + list_slices[1]
    seq2_w_header = '>' + list_slices[2]

    list_lines_seq1 = re.split("\n", seq1_w_header)
    list_lines_seq2 = re.split("\n", seq2_w_header)
    
    seq1_context = ''
    seq2_context = ''

    for eachline in list_lines_seq1:
        if eachline.startswith('>') == 0 :
            if eachline != '':
                seq1_context += eachline
        else:
            uniprotID1 += eachline
                
    for eachline in list_lines_seq2:
        if eachline.startswith('>') == 0 :
            if eachline != '':
                seq2_context += eachline
        else:
            uniprotID2 += eachline

    ## check length equal 
    m = len(seq1_context)
    n = len(seq2_context)

    ## count identity
    gaps_seq1 = 0 
    gaps_seq2 = 0 
    identity_chars = 0 
    for i in range(0, m ,1):
        if seq1_context[i] == seq2_context[i]:
            identity_chars += 1
        if seq1_context[i] == '-':
            gaps_seq1 += 1
        if seq2_context[i] == '-':
            gaps_seq2 += 1
    ## without gap, the length of sequence 
    seq1_wo_gap = m - gaps_seq1
    seq2_wo_gap = n - gaps_seq2
    ## calculate the identity of sequence
    seq1_identity = identity_chars / seq1_wo_gap
    seq2_identity = identity_chars / seq2_wo_gap
    
    
    seq_identity_info = uniprotID1 + ',' +\
                        uniprotID2 + ',' +\
                        str(seq1_identity) + ',' +\
                        str(seq2_identity)
    print(seq_identity_info)
    return 0


def main():
    
    for root, subdir_list, file_list in os.walk('/home/bwang/data/cancerSNP/benchmark/humvar/Pairwise/aligned/'):
        for each_file in file_list:
            if each_file.endswith('.fasta'):
                each_file_abs_path = root + each_file
                ReadIdentity(alignedFasta_file = each_file_abs_path)
    return 0 


if __name__ == '__main__':
    main()