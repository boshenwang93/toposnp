#! /usr/bin/python3

import re
import urllib3

###########################################################
#### API Key: 3b2244872b9561678f73f4a6240cbdcf0908 ########
#### Must remember: 10 Requests per second ################
###########################################################

def DownloadNcbiFasta(NCBI_ID, Assigned_download_folder):

    # assemble the efetch URL
    URL_base = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/"

    # epost for later processing
    URL_epost = URL_base + "epost.fcgi?db=protein&id=" + NCBI_ID

    # efetch for retreive the fasta file
    URL_efetch = URL_base + "efetch.fcgi?db=protein&id=" + NCBI_ID +\
        "&query_key=$key1&WebEnv=$web1" +\
        "&rettype=fasta" +\
        "&retmote=text" +\
        "&api_key=3b2244872b9561678f73f4a6240cbdcf0908"

    # initalize the connection of http
    http = urllib3.PoolManager()

    r = http.request('GET',
                     URL_efetch,
                     timeout=1.0,
                     retries=3,
                     preload_content=False)

    # new fasta file path
    fasta_file_path = Assigned_download_folder + NCBI_ID + '.fasta'
    with open(fasta_file_path, 'wb') as out:
        while True:
            data = r.read()
            if not data:
                break
            out.write(data)

    r.release_conn()

    return 0


# Parse dbSNP missense info
def ParseMissenseInfo(dbsnp_missense_file_path):

    # initalize the list to store the unique protein id
    list_unique_prot_acc = []

    # Open the file handle
    f = open(dbsnp_missense_file_path, 'r')
    for line in f:
        slices_list = re.split(',', line)
        for element in slices_list:
            element = element.strip()
            if re.search("prot", element):
                prot_whole = re.split("\|", element)[-1]
                prot_acc = re.split("=", prot_whole)[-1]
                prot_acc = prot_acc.strip()

                if prot_acc.startswith('NP'):
                    if prot_acc not in list_unique_prot_acc:
                        list_unique_prot_acc.append(prot_acc)
                        print(prot_acc)
                        DownloadNcbiFasta(NCBI_ID= prot_acc, 
                                          Assigned_download_folder='/home/bwang/project/toposnp/data/fasta/')
    f.close()

    return list_unique_prot_acc

ParseMissenseInfo(dbsnp_missense_file_path = '/home/bwang/project/toposnp/script/dbSNP/Only_dbSNP_missense')