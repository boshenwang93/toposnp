#! /usr/bin/python3

import re
import os

##############################################
#### retrieve dbSNP Flat file ################
#### rsID, validation, LOC, allele ###########
##############################################

def read_dbSNP_flat(Flat_file_path, out_raw_file_path):

    # out folder 
    out = open(out_raw_file_path, 'a')

    # read the file content into a single string
    f = open(Flat_file_path, "r")

    for single_line in f:
        # read the rs ID line
        if single_line.startswith("rs"):
            rs_ID = re.split("\|", single_line)[0]
            taxonomy_ID = re.split("\|", single_line)[2]
            snp_class = re.split("\|", single_line)[3]
            
            tmp_info = "\n" + rs_ID + "," + taxonomy_ID + "," + snp_class + ","
            out.write(tmp_info)

         # read SNP allele info
        if single_line.startswith("SNP"):
            allele_info = re.split("\|", single_line)[1].strip()
            allele_change = re.split("=", allele_info)[1]
            tmp_info = allele_change + ","
            out.write(tmp_info)

        # validation info 
        if single_line.startswith("VAL"):
            validated_check_info = re.split("\|", single_line)[1].strip()
            if validated_check_info == "validated=YES":
                out.write("YES,")
            else:
                out.write("NO,")

        # rs SNP location/position
        # may contain several lines
        if single_line.startswith("CTG"):
            assembly = re.split("\|", single_line)[1].strip()
            chr = re.split("\|", single_line)[2].strip()
            pos = re.split("\|", single_line)[3].strip()
            out_position = assembly + "|" + chr + "|" + pos
            
            tmp_info = out_position + ","
            out.write(tmp_info)

        # read the LOC info
        # Attention LOC may contain several lines, use "|" for separate
        if single_line.startswith("LOC"):
            funClass = re.split("\|", single_line)[3].strip()
            function_class = re.split("=", funClass)[1]
            
            if function_class == "missense":
                gene_symbol = re.split("\|", single_line)[1].strip()
                residue = re.split("\|", single_line)[6].strip()
                AA_position = re.split("\|", single_line)[7].strip()
                mrna_acc = re.split("\|", single_line)[8].strip()
                prot_acc = ""

                try:
                    prot_acc += re.split("\|", single_line)[9].strip()
                except:
                    pass

                out_coding_AA_mut_info = gene_symbol + "|" + residue + "|" +\
                    AA_position + "|" + mrna_acc + "|" +\
                    prot_acc + ","
                out.write(out_coding_AA_mut_info)
    f.close()
    out.close()
    return 0

def main(assigned_dbSNP_directory, tmp_file, final_file):

    # iterating the pdb folder
    for root, subdirs, files in os.walk(assigned_dbSNP_directory):
        for file in files:
            file_path = root + file

            if file.endswith(".flat"):
                read_dbSNP_flat(Flat_file_path= file_path, 
                                out_raw_file_path = tmp_file)
    
    ###############################################
    #### filter the raw dbSNP #####################
    #### must human, snp, pass validation #########
    ###############################################
    out = open(final_file, 'w')
    f = open(tmp_file, "r")
    for line in f:
        try:
            taxonomy = re.split(",", line)[1].strip()
            snpClass = re.split(",", line)[2].strip()
            validation = re.split(",", line)[4].strip()

            if re.search("residue", line):
                if taxonomy == "9606":
                    if snpClass == "snp":
                        if validation == "YES":
                            out.write(line)
        except:
            pass
    f.close()
    out.close()
    
    return 0

if __name__ == "__main__":
    main(assigned_dbSNP_directory = "/data/database/dbSNP/B151/",
         tmp_file = 'testRaw',
         final_file = 'Only_dbSNP_missense')