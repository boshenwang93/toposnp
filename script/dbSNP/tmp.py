#! /usr/bin/python3

import re
import os

def read_dbSNP_flat(Flat_file_path):

    list_function_class = [] 

    # read the file content into a single string
    f = open(Flat_file_path, "r")

    for single_line in f:
        # read the LOC info
        # Attention LOC may contain several lines, use "|" for separate
        if single_line.startswith("LOC"):
            funClass = re.split("\|", single_line)[3].strip()
            function_class = re.split("=", funClass)[1]

            if function_class not in list_function_class:
                list_function_class.append(function_class)
    f.close()

    return list_function_class


list_func_class = read_dbSNP_flat(Flat_file_path= '/data/database/dbSNP/B151/ds_flat_ch1.flat')
print(list_func_class)