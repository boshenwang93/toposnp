#! /usr/bin/python3

import re 
import urllib3


def DownloadNcbiFasta(NCBI_ID, Assigned_download_folder):
    
    http = urllib3.PoolManager()

    ## assemble the efetch URL
    URL_base = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/"
    
    # epost for later processing 
    URL_epost = URL_base + "epost.fcgi?db=protein&id=" + NCBI_ID

    # efetch for 
    URL_efetch = URL_base +  "efetch.fcgi?db=protein&id=" + NCBI_ID +\
               "&query_key=$key1&WebEnv=$web1" +\
               "&rettype=fasta" +\
               "&retmote=text" +\
               "&api_key=3b2244872b9561678f73f4a6240cbdcf0908"

    r = http.request( 'GET', 
                      URL_efetch,
                      timeout = 1.0, 
                      retries = 3,
                      preload_content=False)

    
    ## new fasta file path 
    fasta_file_path = Assigned_download_folder + NCBI_ID + '.fasta'
    with open(fasta_file_path, 'wb') as out:
        while True:
            data = r.read()
            if not data:
                break
            out.write(data)

    r.release_conn()
    return 0


def main():

    ## fetch the list of prot acc from prot_acc list 
    list_prot_acc = [] 

    prot_acc_file = '/home/bwang/project/toposnp/script/dbSNP/prot_acc.list'
    f = open(prot_acc_file, 'r')
    for line in f:
        line  = line.strip()
        list_prot_acc.append(line)
    f.close()
    
    for each_prot_acc in list_prot_acc:
        DownloadNcbiFasta(NCBI_ID = each_prot_acc,
                          Assigned_download_folder = '/home/bwang/project/toposnp/data/NCBI/fasta/')
    
    return 0

if __name__ == "__main__":
    main()