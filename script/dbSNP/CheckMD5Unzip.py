#! /usr/bin/python3
import os 

#############################
#### MD5 checking ###########
#############################
def checkMD5 (local_dbSNP_folder):
    ## iterate local dbSNP folder, compare reference md5 with calculated md5
    for root, dirs, files in os.walk(local_dbSNP_folder):
        os.chdir(root)
        ## use reference md5 for checking
        for single_file in files:
            if single_file.endswith('.md5'): 
                cmd = 'md5sum -c ' + single_file 
                os.system(cmd)
    ## terminate the function
    return 0 

#############################
#### unzip .gz files ########
#############################
def unzip (local_dbSNP_folder):
    ## iterate local dbSNP folder, unzip .gz and No deletion
    for root, dirs, files in os.walk(local_dbSNP_folder):
        for single_file_name in files:
            single_file_path = os.path.join(root, single_file_name)

            if single_file_path.endswith('.gz'): 
                os.chdir(root)
                ## decompress and Keep original .gz file
                cmd = 'gunzip -d -k ' + single_file_path
                os.system(cmd)

    ## terminate the function
    return 0


def main(assigned_dbSNPflat_folder):
    ## Checking with md5 record 
    ## print log on screen
    checkMD5(local_dbSNP_folder = assigned_dbSNPflat_folder)

    ## unzip .gz files without deletion
    unzip(local_dbSNP_folder = assigned_dbSNPflat_folder)

    return 0 


if __name__ == '__main__':
    main(assigned_dbSNPflat_folder = '/data/database/dbSNP/B151/')