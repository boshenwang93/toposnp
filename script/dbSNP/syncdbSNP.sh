
############################################################################
# You should CHANGE THE NEXT THREE LINES to suit your local setup
############################################################################
MIRRORDIR=/data/database/dbSNP/B151/            # your top level rsync directory
LOGFILE=/data/database/dbSNP/dbSNPsynclogs               # file for storing logs
RSYNC=/usr/bin/rsync                            # location of local rsync

##########################################################################################
# set the PDB node and Port parameter 
##########################################################################################
SERVER=rsync://ftp.ncbi.nlm.nih.gov                                # dbSNP server name

############################################################################
# Rsync only the PDB format coordinates  /pub/pdb/data/structures/divided/pdb (Aproximately 20 GB)
############################################################################
${RSYNC} --copy-links --recursive --times --verbose ${SERVER}/snp/organisms/human_9606_b151_GRCh38p7/ASN1_flat/ $MIRRORDIR > $LOGFILE 2>/dev/null
