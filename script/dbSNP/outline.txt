###########################################################
#### Parse dbSNP FLAT format file #########################
#### Written by Boshen Wang, March 2018 ###################
#### Documentation: header of FLAT file ###################
###########################################################

###########################################################
#### reason for choice FLAT format ########################
#### NCBI SNP FTP provide XML/FLAT/VCF/ format ############
#### XML comperhensive, But not compact ###################
#### VCF has raw info, No mRNA/AA, Alt Res, etc. ##########
#### BED records chromosome positive, direction ###########
#### chr_rpts records raw contig/date info per chromosome##
#### database folder No Update, switch to JSON ############
#### rs_fasta records raw rsSNP DNA seq info ##############
###########################################################

###########################################################
#### Process Outline ######################################
#### 1. Download Human dbSNP FLAT file with md5 checking ##
#### 2. Retrieve info and convert to JSON format ##########
######## Attention, NCBI has JSON in trial, ###############
######## Ignore it for compactness ########################
###########################################################


###########################################################
#### STEP 1. ##############################################
#### Download latest dbSNP human FLAT file ################
#### with md5 checking ####################################
#### with unzip .gz file without deletion##################
###########################################################

['intron-variant', 
'reference', 
'missense', 
'nc-transcript-variant', 
'upstream-variant-2KB', 
'utr-variant-5-prime', 
'utr-variant-3-prime', 
'downstream-variant-500B',
 'synonymous-codon', 
 'splice-acceptor-variant', 
 'stop-gained', 
 'splice-donor-variant',
  'stop-lost', 
  'cds-indel', 
  'frameshift-variant']


###########################################################
#### StEP 2. ##############################################
#### Retrieve raw MISSENSE SNPs info ######################
#### Parse Flat file from dbSNP ###########################
#### Filter raw flat info #################################
###########################################################