#! /usr/bin/python3
import os 
import re


# Common amino acids
list_common_aminoacids= [        
        "ALA",
        "ARG",
        "ASN",
        "ASP",
        "CYS",
        "GLN",
        "GLU",
        "GLY",
        "HIS",
        "ILE",
        "LEU",
        "LYS",
        "MET",
        "PHE",
        "PRO",
        "SER",
        "THR",
        "TRP",
        "TYR",
        "VAL",
        ## Some uncommon/undetermined ABBV
        "PYL", 
        "SEC",
        "DA", "DC", "DG", "DT", "DU", "DI",
        "A", "C", "G", "U", "I",
        "UNK",
        "N"
]



def ParsePDBresidue(pdb_file_path):

    PDB_id = ''

    list_unique_unrecognized_residue = [] 

    ## obtain list of chain, PDB ID
    f = open(pdb_file_path, 'r')

    for line in f:
        if line.startswith("HEADER"):
                PDB_id += line[62:66]

        if line.startswith("ATOM"):
            # residue name
            residue_tri = ""
            for i in range(17, 20, 1):
                residue_tri += line[i]
            residue_tri = residue_tri.strip()
            residue_tri = residue_tri.upper()
            
            if residue_tri not in  list_common_aminoacids:
                if residue_tri not in list_unique_unrecognized_residue:
                    list_unique_unrecognized_residue.append(residue_tri)
    f.close()

    return PDB_id, list_unique_unrecognized_residue

def main():
    ## assign root folder 
    PDB_folder = '/data/database/PDB/'
    ## obtain the paths for divided folder 
    list_dirs = [] 
    for root, dirs, files in os.walk(PDB_folder):
        for subdir in dirs:
            subdir_abs_path = root + subdir + '/'
            list_dirs.append(subdir_abs_path)

    ## Collect the fasta seq info 
    for each_subdir in list_dirs:
        for root, dirs, files in os.walk(each_subdir):
            for each_file in files:
                if each_file.startswith('pdb') == 1:  
                    file_abs_path = each_subdir + each_file

                    PDB_id, list_unReco_res = ParsePDBresidue(pdb_file_path = file_abs_path)
                    if list_unReco_res != [] :
                        print(PDB_id, list_unReco_res)
    return 0

if __name__ == "__main__":
    main()