#! /usr/bin/python3
import os 
import re

## assign root folder 
PDB_folder = '/data/database/PDB/'

## obtain the paths for divided folder 
list_dirs = [] 
for root, dirs, files in os.walk(PDB_folder):
    for subdir in dirs:
        subdir_abs_path = root + subdir + '/'
        list_dirs.append(subdir_abs_path)
    
## unzip all .gz files 
for each_subdir in list_dirs:
    for root, dirs, files in os.walk(each_subdir):
        print(each_subdir)
        for each_file in files:
            file_abs_path = each_subdir + each_file
            if file_abs_path.endswith('.gz'):
                cmd = 'gzip -d ' + file_abs_path
                os.system(cmd)