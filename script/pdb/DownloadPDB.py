#! /usr/bin/python3

import os
import re 

def RetrievePDB(BestMappingFolder):
    
    ## initialize the list for non-redundant PDB
    list_NR_pdb = []

    for root, subdirs, files in os.walk(BestMappingFolder):
        for each_file in files:
            pdb_info = re.split("_>", each_file)[-1]
            pdb_info = pdb_info[0:4]
            if pdb_info not in list_NR_pdb:
                list_NR_pdb.append(pdb_info)
    return list_NR_pdb 


def DownloadPdb(pdb, download_dir):
    rcsb_link = "https://files.rcsb.org/download/" + pdb + ".pdb"
    command_text = "wget " + rcsb_link + " -P " + download_dir 
    os.system(command_text)
    return 0 
 
def main(out_dir):
    list_NR_pdb = RetrievePDB(BestMappingFolder = '/data/database/B151TopoSNP/BestMapping/')
    for each_pdb in list_NR_pdb:
        DownloadPdb(pdb = each_pdb,
                    download_dir = out_dir)
    return 0

if __name__ == '__main__':
    main(out_dir = '/data/database/B151TopoSNP/pdb/raw_pdb/' )