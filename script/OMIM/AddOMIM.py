#! /usr/bin/python3
import re 

dic_rsSNP_OMIM = {}
list_unique_rsSNP = []
f = open('OMIM_entry.csv', 'r')
for line in f:
    line = line.strip()
    rsSNP = re.split(",", line)[0]
    OMIM = re.split(",", line)[-6]
    
    pair = rsSNP + OMIM
    if pair not in list_unique_rsSNP:
        list_unique_rsSNP.append(pair)
        dic_rsSNP_OMIM[rsSNP] = OMIM
f.close()

f = open('/home/bwang/project/toposnp/script/MysqlDBcsv/PDBNP2rsSNPSASA.csv', 'r')
for line in f:
    line = line.strip()
    rsSNP = re.split(",", line)[3]

    try:
        out_line = line + "," + dic_rsSNP_OMIM[rsSNP]
        print(out_line)
    except:
        out_line = line + ",Neutral"
        print(out_line)
f.close()