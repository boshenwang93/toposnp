#! /usr/bin/python3

import re 
import os 

def RetrievePocCenter(Pocket_pdb_file, new_summary_file):
    ## Initialize list of Pocket Center
    list_center = []
    
    f = open(Pocket_pdb_file, 'r')
    for line in f:
        line_record_identify = ""
        # column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":
            # atom x, y, z
            poc_id = ""
            for i in range(22, 30, 1):
                poc_id += line[i]
            poc_id = poc_id.strip()

            x = ""
            for i in range(30, 38, 1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38, 46, 1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46, 54, 1):
                z += line[i]
            z = z.strip()

            out_entry = poc_id + ',' + x + ',' + y + ',' + z
            list_center.append(out_entry)
    f.close()

    out = open(new_summary_file, 'w')
    for e in list_center:
        out_line = e + "\n"
        out.write(out_line)
    out.close()

    return 0

def main(Assigned_folder, new_poc_center_folder):

    for root, subdirs, files in os.walk(Assigned_folder):
        for every_file in files:
            if every_file.endswith("poc.pdb"):
                file_path = root + every_file
                pdb_id = every_file[-12:-8]
                new_file = new_poc_center_folder + pdb_id + '.poc.center'
                RetrievePocCenter(Pocket_pdb_file = file_path,
                                  new_summary_file = new_file)

    return 0

if __name__ == "__main__":
    main(Assigned_folder = '/data/database/B151TopoSNP/pdb/single_conform/',
         new_poc_center_folder = '/data/database/B151TopoSNP/pdb/poc/')