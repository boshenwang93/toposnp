#! /usr/bin/python3

import re 

f = open("Poc_Summ", 'r')
for line in f:
    pdb = re.split(',', line)[0]
    poc_id = re.split(',', line)[1]
    
    ## obtain all chains 
    count = len(re.split(",", line))
    list_unique_chain = []
    for i in range(2, count):
        res_w_chain = re.split(',', line)[i]
        chain = re.split("\|", res_w_chain)[0].strip()
        
        if chain not in list_unique_chain:
            if chain != "":
                list_unique_chain.append(chain)
    
    ## obtain each chain Hash Table
    for each_chain in list_unique_chain:
        pdb_w_chain = pdb + "_" + each_chain 
        out_line = pdb_w_chain + ',' + poc_id

        for i in range(2, count):
            res_w_chain = re.split(',', line)[i]
            chain = re.split("\|", res_w_chain)[0].strip()
            if chain == each_chain:
                out_line += ',' + res_w_chain
        
        print(out_line)
f.close()