#! /usr/bin/python3

import os
import re

def RetrievePDB(BestMappingFolder):
    
    ## initialize the list for non-redundant PDB
    list_NR_pdb_chain = []

    for root, subdirs, files in os.walk(BestMappingFolder):
        for each_file in files:
            pdb_info = re.split("_>", each_file)[-1]
            pdb_id = pdb_info[0:4]
            chain = pdb_info[5]

            pdb_chain_id = pdb_id + "_" + chain
            if pdb_info not in list_NR_pdb_chain:
                list_NR_pdb_chain.append(pdb_chain_id)
    return list_NR_pdb_chain

def main():
    list_NR_pdb_chain = RetrievePDB(BestMappingFolder = '/data/database/B151TopoSNP/BestMapping/')
    for e in list_NR_pdb_chain:
        print(e)
    return 0 

if __name__ == '__main__':
    main()