#! /usr/bin/python3

import re
import os
from multiprocessing import Process, Pool

###########################################
#### Hash Table: PocID => list Res ########
###########################################

def Hash_pocid_residues(castp_output_file):
    ## get pdb id 
    pdb = castp_output_file[-8:-4]

    ## initalize dictionary 
    dic_poc_res = {}
    list_unique_poc = []
    list_unique_residue = []

    f = open(castp_output_file, "r")
    for line in f:
        if line.startswith("ATOM"):

            # chain identifier
            chain = line[21]

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            ## Poc ID 
            poc_id = ""
            for i in range(66,72, 1):
                poc_id += line[i]
            poc_id = poc_id.strip()

            if poc_id not in list_unique_poc:
                list_unique_poc.append(poc_id)
            
            residue_entry = poc_id + ',' + chain + ',' +\
                            residue_seq_number + ',' + residue
            if residue_entry not in list_unique_residue:
                list_unique_residue.append(residue_entry)
    f.close()

    for each_poc in list_unique_poc:
        tmp_list_residue = []
        for each_residue in list_unique_residue:
            residue_pocid = re.split(",", each_residue)[0]
            
            res_summ = re.split(",", each_residue)[1] + "|" +\
                       re.split(",", each_residue)[2]
            if residue_pocid == each_poc:
                tmp_list_residue.append(res_summ)
        dic_poc_res[each_poc] = tmp_list_residue
    

    ## out string 
    out_info = ""
    for k, v in dic_poc_res.items():
        out_line = pdb + ',' + k + ','
        for each_res in v:
            out_line += each_res + ','
        print(out_line)
        out_line += "\n"
        out_info += out_line

    return out_info


def main(castp_out_dir):
    for root, subdirs, files in os.walk(castp_out_dir):
        for every_file in files:
            if every_file.endswith(".poc"):
                file_path = root + every_file
                Hash_pocid_residues(castp_output_file = file_path)
    return 0 

## multiple process to calculated
if __name__ == "__main__":
    main(castp_out_dir = '/data/database/B151TopoSNP/pdb/single_conform/')