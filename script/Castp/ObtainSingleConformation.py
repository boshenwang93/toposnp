#! /usr/bin/python3
import re
import os

# check the pdb which contains multiple conformations

def check_pdb(pdb_file_path, new_folder):

    pdb = re.split("\.pdb", pdb_file_path)[0]
    pdb = re.split("/", pdb)[-1].strip()

    label_multiconform = 0
    f = open(pdb_file_path, 'r')
    for line in f:
        line_record_identify = ""
        # column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "MODEL":
            label_multiconform += 1
    f.close()

    if label_multiconform == 0:
        command_line = 'cp ' + pdb_file_path + ' ' + new_folder
        os.system(command_line)
    else:
        single_conformation_file_path = new_folder + pdb + '.pdb'
        print(pdb_file_path, 'MultiConformation')

        out = open(single_conformation_file_path, 'w')

        f = open(pdb_file_path, 'r')
        count = 0
        for line in f:
            line_record_identify = ""
            # column 1-6 is the identfication for the line record
            for i in range(0, 6, 1):
                line_record_identify += line[i]
            line_record_identify = line_record_identify.strip()

            if line_record_identify == "MODEL":
                count += 1

            if count < 2:
                out.write(line)
        f.close()
        out.close()
    return 0


def main(in_dir, out_dir):

    for root, subdirs, files in os.walk(in_dir):
        for file in files:
            if file.endswith('pdb'):
                file_path = root + file
                check_pdb(pdb_file_path=file_path,
                          new_folder=out_dir)
    return 0


if __name__ == "__main__":
    main(in_dir='/data/database/B151TopoSNP/pdb/raw_pdb/',
         out_dir='/data/database/B151TopoSNP/pdb/single_conform/')
