#! /usr/bin/python3

import re
import os

def main(castp_out_dir):
    list_pdb = []

    for root, subdirs, files in os.walk(castp_out_dir):
        for file in files:
            if file.endswith(".4.contrib"):
                pdb_id = file[-14:-10]
                if pdb_id not in list_pdb:
                    poc_name = pdb_id + '.poc.pdb'
                    pdb_file = pdb_id + '.pdb'

                    new_zip_file = root + pdb_id + '.zip'

                    # Make sure that poc file exist
                    if poc_name in files:
                        if pdb_file in files:
                            poc_file = root + poc_name
                            pdb_file = root + pdb_file
                            zip_cmd = 'zip ' + new_zip_file + "\t" +\
                                      poc_file + "\t" + pdb_file
                            os.system(zip_cmd)
    return 0 

## multiple process to calculated
if __name__ == "__main__":
    main(castp_out_dir = '/data/database/B151TopoSNP/pdb/single_conform/')