#! /usr/bin/python3

import re
import os
from multiprocessing import Process, Pool

###########################################
#### Reading Poc information ##############
###########################################

def read_castp_poc_output(castp_output_file):

    list_poc_residue = []

    pdb = castp_output_file[-8:-4]

    f = open(castp_output_file, "r")
    for line in f:
        if line.startswith("ATOM"):

            # chain identifier
            chain = line[21]

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            residue_id = pdb + "," + chain + "," + residue_seq_number + "," + residue
            if residue_id not in list_poc_residue:
                list_poc_residue.append(residue_id)
    f.close()

    return list_poc_residue


###########################################
#### Capture Buried Residues     ##########
###########################################

def get_buried_residues(castp_output_file):

    # initialize the list for all residues, buried residues, SASA info
    list_unique_residue = []
    list_buried_residue = []
    list_sasa = []

    PDB = castp_output_file[-14:-10]

    f = open(castp_output_file, "r")
    list_atom_entry = []
    for line in f:
        if line.startswith("ATOM"):
            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip()

            # atom's sasa
            atom_sasa = ""
            for i in range(39, 46, 1):
                atom_sasa += line[i]
            atom_sasa = atom_sasa.strip()

            residue_id = PDB + "," + chain + "," + residue_seq_number + "," + residue

            atom_entry = residue_id + "," + atom + "," + atom_sasa
            list_atom_entry.append(atom_entry)

            if residue_id not in list_unique_residue:
                list_unique_residue.append(residue_id)

    for residue in list_unique_residue:
        residue_sasa = 0

        for atom_entry in list_atom_entry:
            atom_entry_id = re.split(",", atom_entry)[0] + "," +\
                re.split(",", atom_entry)[1] + "," +\
                re.split(",", atom_entry)[2] + "," +\
                re.split(",", atom_entry)[3]

            if residue == atom_entry_id:
                atom_sasa = float(re.split(",", atom_entry)[5])
                residue_sasa += atom_sasa

        # keep 4 decimal
        residue_sasa = "%.4f" % residue_sasa
        SASA_output = residue + "," + residue_sasa
        list_sasa.append(SASA_output)

        if float(residue_sasa) == 0.0:
            if residue not in list_buried_residue:
                list_buried_residue.append(residue)
    f.close()

    return list_buried_residue, list_sasa, list_unique_residue


def MergeTwoSteps(PDB):

    out_file = '/data/database/B151TopoSNP/pdb/single_conform/' + PDB + '.SASA'
    out = open(out_file, 'w')

    poc_file_path = '/data/database/B151TopoSNP/pdb/single_conform/' + PDB + '.poc'
    sasa_file_path = "/data/database/B151TopoSNP/pdb/single_conform/" + PDB + '.4.contrib'

    list_poc_residue = read_castp_poc_output(castp_output_file = poc_file_path)
    list_buried_residue, list_sasa, list_all_residue = get_buried_residues(castp_output_file=sasa_file_path)
    
    for every_residue_w_sasa in list_sasa:
        out_line = every_residue_w_sasa + ","

        entry_slices = re.split(",", every_residue_w_sasa)
        residue_id = entry_slices[0] + "," + entry_slices[1] + "," +\
                     entry_slices[2] + "," + entry_slices[3]
        
        if residue_id not in list_poc_residue:
            if residue_id not in list_buried_residue:
                out_line +=  "Surface"
            else:
                out_line +=  "Buried"
        else:
            if residue_id not in list_buried_residue:
                out_line +=  "Pocket"
            else:
                out_line  +=  "PB"
        out_line += "\n"
        out.write(out_line)
    out.close()
    return 0 

MergeTwoSteps(PDB = '9JDW')

def main(castp_out_dir):
    list_pdb = []

    for root, subdirs, files in os.walk(castp_out_dir):
        for file in files:
            if file.endswith(".4.contrib"):
                pdb_id = file[-14:-10]
                if pdb_id not in list_pdb:
                    poc_name = pdb_id + '.poc'
                    # Make sure that poc file exist
                    if poc_name in files:
                        SASA_name = pdb_id + '.SASA'
                        if SASA_name not in files:
                            list_pdb.append(pdb_id)
                        
    with Pool(processes= 10) as pool:
        pool.map(MergeTwoSteps, list_pdb)

    return 0 

## multiple process to calculated
if __name__ == "__main__":
    main(castp_out_dir = '/data/database/B151TopoSNP/pdb/single_conform/')