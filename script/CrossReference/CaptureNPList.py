#! /usr/bin/python3
import re
import os 

def CaptureNPList(Best_Identity_folder):

    ## initialize the list for storing NP
    list_NP = [] 
    
    ## visit the Best identity mapping folder 
    for root, subdirs, files in os.walk(Best_Identity_folder):
        for each_file in files:
            NP_id = re.split("_>", each_file)[1].strip()
            if NP_id not in list_NP:
                list_NP.append(NP_id)
    
    return list_NP

NP_List = CaptureNPList(Best_Identity_folder = '/data/database/B151TopoSNP/BestMapping/')

for each_NP in NP_List:
    print(each_NP)

