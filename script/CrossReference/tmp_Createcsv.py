#! /usr/bin/python3

import re 
import os 

###########################################################
###########################################################
## NP => list of Uniprots (reviewed and unreviewed)
def NP2Uniprot(NP_uniprot_table_file):
    ## initialize the list for unique NPs
    list_unique_NPs = []
    
    ## first iterative to fetch unique NPs
    f = open(NP_uniprot_table_file, 'r')
    for line in f:
        all_NPs = re.split("\t", line)[0].strip()
        list_NPs = re.split(",", all_NPs)
        for each_NPs in list_NPs:
            each_NPs = each_NPs.strip()            
            if each_NPs not in list_unique_NPs:
                list_unique_NPs.append(each_NPs)
    f.close()
    
    ## Obtain Key => Value   NP => List of uniprots
    for every_NP in list_unique_NPs:
        ## initialize the current NP corresponding list of uniprots 
        list_corresponding_uniprots = []

        ## Read the file
        f = open(NP_uniprot_table_file, 'r')
        for line in f:
            all_NPs = re.split("\t", line)[0].strip()
            uniprot = re.split("\t", line)[1].strip()

            list_NPs = re.split(",", all_NPs)
            for each_NPs in list_NPs:
                each_NPs = each_NPs.strip()
                if every_NP == each_NPs:
                    if uniprot not in list_corresponding_uniprots:
                        list_corresponding_uniprots.append(uniprot)
        f.close()

        ## print out the Key => Value
        out_line = every_NP + ","
        for each_uniprot in list_corresponding_uniprots:
            out_line += each_uniprot + ","
        print(out_line)

    return 0
    
###########################################################
###########################################################
## Uniprot => list of NPs (Alternatibe Splicing)
def Uniprot2NP(NP_uniprot_table_file):
    ## initialize the list of uniprots
    list_unique_uniprots = []
    
    ## first iterative to fetch all unique uniprots
    f = open(NP_uniprot_table_file, 'r')
    for line in f:
        uniprot = re.split("\t", line)[1].strip()
        if uniprot not in list_unique_uniprots:
            list_unique_uniprots.append(uniprot)
    f.close()

    ## Obatin Key => Value   Uniprot => list of NPs
    for every_uniprot in list_unique_uniprots:
        ## initialize the list for corresponding NPs
        list_corresponding_NPs = []

        ## visit the file handle
        f = open(NP_uniprot_table_file, 'r')
        for line in f:
            all_NPs = re.split("\t", line)[0].strip()
            uniprot = re.split("\t", line)[1].strip()

            list_NPs = re.split(",", all_NPs)
            for each_NPs in list_NPs:
                each_NPs = each_NPs.strip()
                if uniprot == every_uniprot:
                    list_corresponding_NPs.append(each_NPs)
        f.close()

        ## print out 
        out_line = every_uniprot + ","
        for each_NP in list_corresponding_NPs:
            out_line += each_NP
        print(out_line)

    return 0

# Uniprot2NP(NP_uniprot_table_file = '/home/bwang/project/toposnp/script/CrossReference/NP_UniProt.tab')


###########################################################
###########################################################
## NP => HGNC Gene Symbols
def NP2HGNC(mappable_rsSNP_file, out_file):

    ## initialize the dictionary NP => HGNC
    dictionary_NP_HGNC = {}
    list_unique_NP = []

    ## first visit to obtain unique NPs, open the file handle 
    f = open(mappable_rsSNP_file, 'r')
    for line in f:
        line = line.strip()
        whole_prot_entry = re.split(",", line)[-1]
        HGNC = re.split("\|", whole_prot_entry)[0]
        NP_id = re.split("\|", whole_prot_entry)[-1]
        NP_id = re.split("=", NP_id)[-1]

        if NP_id not in list_unique_NP:
            list_unique_NP.append(NP_id)
    f.close()
    
    ## Get list of HGNC for each NP
    out = open(out_file, 'w')
    for each_NP in list_unique_NP:

        ## initialize the list of corresponding HGNC for current NP
        list_corresponding_HGNC = []
        
        f = open(mappable_rsSNP_file, 'r')
        for line in f:
            line = line.strip()
            whole_prot_entry = re.split(",", line)[-1]
            HGNC = re.split("\|", whole_prot_entry)[0]
            NP_id = re.split("\|", whole_prot_entry)[-1]
            NP_id = re.split("=", NP_id)[-1]

            if NP_id == each_NP:
                if HGNC not in list_corresponding_HGNC:
                    list_corresponding_HGNC.append(HGNC)
        f.close()
        
        dictionary_NP_HGNC[each_NP] = list_corresponding_HGNC

        ## print out 
        out_line = each_NP + ","
        for e in list_corresponding_HGNC:
            out_line += e + ","
        out_line += "\n"
        out.write(out_line)
    out.close()

    return dictionary_NP_HGNC

# NP2HGNC(mappable_rsSNP_file = '/home/bwang/project/toposnp/script/CrossReference/mappable_rsSNP', 
#         out_file = "NP2HGNC.csv")

###########################################################
###########################################################
## NP => list of rsSNP
def NP2rsSNP(mappable_rsSNP_file, out_file):

    ## initialize the dictionary NP => rsSNP
    dictionary_NP_rsSNP = {}
    list_unique_NP = []

    ## first visit to obtain unique NPs, open the file handle 
    f = open(mappable_rsSNP_file, 'r')
    for line in f:
        line = line.strip()
        whole_prot_entry = re.split(",", line)[-1]
        NP_id = re.split("\|", whole_prot_entry)[-1]
        NP_id = re.split("=", NP_id)[-1]

        if NP_id not in list_unique_NP:
            list_unique_NP.append(NP_id)
    f.close()
    
    ## Get list of rsSNP for each NP
    out = open(out_file, 'w')
    for each_NP in list_unique_NP:

        ## initialize the list of corresponding rsSNP for current NP
        list_corresponding_rsSNP = []
        
        f = open(mappable_rsSNP_file, 'r')
        for line in f:
            line = line.strip()
            rsSNP = re.split(",", line)[0].strip()
            whole_prot_entry = re.split(",", line)[-1]
            NP_id = re.split("\|", whole_prot_entry)[-1]
            NP_id = re.split("=", NP_id)[-1]

            if NP_id == each_NP:
                if rsSNP not in list_corresponding_rsSNP:
                    list_corresponding_rsSNP.append(rsSNP)
        f.close()
        
        dictionary_NP_rsSNP[each_NP] = list_corresponding_rsSNP

        ## print out 
        out_line = each_NP + ","
        for e in list_corresponding_rsSNP:
            out_line += e + ","
        out_line += "\n"
        out.write(out_line)
    out.close()

    return dictionary_NP_rsSNP

# NP2rsSNP(mappable_rsSNP_file = '/home/bwang/project/toposnp/script/CrossReference/mappable_rsSNP', 
#         out_file = "NP2rsSNP.csv").


def HGNC2NP(NP2HGNC_csv_file):

    list_unique_HGNC = []

    f = open(NP2HGNC_csv_file, 'r')
    for line in f:
        line = line.strip()
        HGNC = re.split(",", line)[1]
        if HGNC not in list_unique_HGNC:
            list_unique_HGNC.append(HGNC)
    f.close()
    
    for every_HGNC in list_unique_HGNC:
        list_corresponding_NPs = []

        f = open(NP2HGNC_csv_file, 'r')
        for line in f:
            line = line.strip()
            NP = re.split(",", line)[0]
            HGNC = re.split(",", line)[1]
            if HGNC == every_HGNC:
                if NP not in list_corresponding_NPs:
                    list_corresponding_NPs.append(NP)
        f.close()

        out_line = every_HGNC + ","
        for e in list_corresponding_NPs:
            out_line += e + ","
        print(out_line)

    return 0 

# HGNC2NP(NP2HGNC_csv_file = 'NP2HGNC.csv')


def rsSNP2HGNC(mappable_rsSNP_file):
    
    list_unique_rsSNP_HGNC_pair = []

    f = open(mappable_rsSNP_file, 'r')
    for line in f:
        line = line.strip()
        rsSNP = re.split(",", line)[0]
        prot_whole_entry = re.split(",", line)[-1]
        HGNC = re.split("\|", prot_whole_entry)[0]

        pair = rsSNP + "," + HGNC
        if pair not in list_unique_rsSNP_HGNC_pair:
            list_unique_rsSNP_HGNC_pair.append(pair)
            print(pair)
    f.close()

    return 0

# rsSNP2HGNC(mappable_rsSNP_file = '/home/bwang/project/toposnp/script/CrossReference/mappable_rsSNP')


def CheckRedundant(rsSNP2HGNC_csv_file):

    list_unique_rsSNP = []

    f = open(rsSNP2HGNC_csv_file)
    for line in f:
        line = line.strip()
        HGNC = re.split(",", line)[1].strip()
        if HGNC != "?":
            print(line)
    f.close()
    

    return 0

# CheckRedundant(rsSNP2HGNC_csv_file = 'rsSNP2HGNC.csv')


def NP2PDB(Best_Mapping_Folder):
    ## initialize the list for storing NP
    list_NP = []
    list_NP_PDB_pair = []

    dic_NP_iden = {}
    
    ## visit the Best identity mapping folder 
    for root, subdirs, files in os.walk(Best_Mapping_Folder):
        for each_file in files:
            iden_score = re.split("_>", each_file)[0].strip()
            iden_score = float(iden_score)
            iden_score = "{0:.3f}".format(iden_score)

            NP_id = re.split("_>", each_file)[1].strip()
            PDB_chain = re.split("_>", each_file)[2].strip()
            PDB_chain = PDB_chain[0:6]

            if NP_id not in list_NP:
                list_NP.append(NP_id)
                dic_NP_iden[NP_id] = iden_score
            
            NP_PDB_pair = NP_id + "," + PDB_chain
            if NP_PDB_pair not in list_NP_PDB_pair:
                list_NP_PDB_pair.append(NP_PDB_pair) 
    
    for every_NP in list_NP:
        list_corresponding_PDB = []
        for pair in list_NP_PDB_pair:
            NP = re.split(",", pair)[0]
            PDB = re.split(",", pair)[1]
            if NP == every_NP:
                list_corresponding_PDB.append(PDB)
        
        out_line = dic_NP_iden[every_NP] + "," + every_NP + ","
        for e in list_corresponding_PDB:
            out_line += e + ","
        print(out_line)

    return 0

# NP2PDB(Best_Mapping_Folder = '/data/database/B151TopoSNP/BestMapping/')

def Uniprot2HGNC(Uniprot2NP_csv_file, NP2HGNC_csv_file):

    dic_NP_HGNC = {}
    f = open(NP2HGNC_csv_file, 'r')
    for line in f:
        line = line.strip()
        NP = re.split(",", line)[0].strip()
        HGNC = re.split(",", line)[1].strip()
        dic_NP_HGNC[NP] = HGNC
    f.close()

    f = open(Uniprot2NP_csv_file, 'r')
    for line in f:
        line = line.strip()
        uniprot = re.split(",", line)[0].strip()
        NPlist =  re.split(",", line)[1].strip()
        firstNP = re.split("NP", NPlist)[1]
        NP = "NP" + firstNP
        out_line = uniprot + "," + dic_NP_HGNC[NP]
        print(out_line)
    f.close()

    return 0 

# Uniprot2HGNC(Uniprot2NP_csv_file = '/home/bwang/project/toposnp/script/MysqlDBcsv/Uniprot2NP.csv',
#              NP2HGNC_csv_file = '/home/bwang/project/toposnp/script/MysqlDBcsv/NP2HGNC.csv')

# f = open('/home/bwang/project/toposnp/script/MysqlDBcsv/HGNC2NP.csv', 'r')
# for line in f:
#     line = line.strip()
#     list_line_pieces = re.split(",", line)
#     HGNC_id = list_line_pieces[0].strip()
#     for i in range(1, len(list_line_pieces)-1 ):
#         out_line = HGNC_id + "," + list_line_pieces[i]
#         print(out_line)
# f.close()


# f = open('/home/bwang/project/toposnp/script/MysqlDBcsv/NP2PDB.csv', 'r')
# for line in f:
#     line = line.strip()
#     line_pieces = re.split(",", line)
#     count = len(line_pieces)
#     identity_score = line_pieces[0]
#     np = line_pieces[1]
#     for i in range(2, count-1):
#         out_line = identity_score + "," + np + "," + line_pieces[i]
#         print(out_line)
# f.close()

list_unique_pdb = []
f = open('/home/bwang/project/toposnp/script/MysqlDBcsv/NP2PDB.csv', 'r')
for line in f:
    line = line.strip()
    line_splices = re.split(",", line)
    coverage = line_splices[0]
    NP = line_splices[1]
    pdb_w_chain = line_splices[2]
    pdb = re.split("_", pdb_w_chain)[0]
    chain =  re.split("_", pdb_w_chain)[1]

    out_line = pdb + "," + NP + "," + chain + "," + coverage
    print(out_line)

f.close()