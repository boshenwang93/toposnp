#! /usr/bin/python3 
import re 

dic_HGNC_hgncIndex = {}

f = open('HGNC.txt', 'r')
for line in f:
    line = line.strip()
    try:
        hgnc_index = re.split("\t", line)[0]
        hgnc_index = re.split(":", hgnc_index)[1]

        hgnc_symbol = re.split("\t", line)[1]
        dic_HGNC_hgncIndex[hgnc_symbol] = hgnc_index
    except:
        pass
f.close()

# f = open('/home/bwang/project/toposnp/script/MysqlDBcsv/NP2HGNC.csv', 'r')
# for line in f:
#     line = line.strip()
#     HGNC_symbol = re.split(",", line)[1]

#     try:
#         out_line = line + dic_HGNC_hgncIndex[HGNC_symbol]
#         print(out_line)
#     except:
#         pass
# f.close()

########################
dic_NP_HGNC ={}
f = open('/home/bwang/project/toposnp/script/MysqlDBcsv/NP2HGNC.csv' , 'r')
for line in f:
    line = line.strip()
    NP = re.split(",", line)[0]
    HGNC = re.split(",", line)[1]
    dic_NP_HGNC[NP] = HGNC
f.close()

###########################
dic_NP_Uniprot = {}
f = open('/home/bwang/project/toposnp/script/MysqlDBcsv/NP2Uniprot.csv', 'r')
for line in f:
    line = line.strip()
    slices = re.split(",", line)
    count = len(slices)
    NP = slices[0]
    Uniprot = slices[1]

    dic_NP_Uniprot[NP] = Uniprot
f.close()

#################################
### PDB TO PFAM ftp://ftp.ebi.ac.uk/pub/databases/msd/sifts/text/pdb_chain_pfam.lst
dic_PDB_Pfam = {}
f = open('/home/bwang/project/toposnp/script/CrossReference/pdb_chain_pfam.lst', 'r')
for line in f:
    line = line.strip()
    slices = re.split("\t", line)
    pdb = slices[0].upper()
    chain = slices[1].upper()
    pdb_chain = pdb + "_" + chain
    pfam = slices[3]
    dic_PDB_Pfam[pdb_chain] = pfam
f.close()

list_unique_PDBNP = []
f = open('/home/bwang/project/toposnp/script/MysqlDBcsv/PDBNP2SASAOMIM.csv', 'r')
for line in f:
    line = line.strip()
    PDBNP = re.split(",", line)[0]
    if PDBNP not in list_unique_PDBNP:
        list_unique_PDBNP.append(PDBNP)
f.close()
for e in list_unique_PDBNP:
    slices = re.split("_", e)
    PDB = slices[0]
    chain = slices[1]
    NP = slices[2] + "_" + slices[3]
    pdb_chain = PDB + "_" + chain

    out_line = e + "," + PDB + ","  + NP

    try:    
        HGNC = dic_NP_HGNC[NP]
        out_line += "," + HGNC + "," + dic_HGNC_hgncIndex[HGNC] + "," +\
                    dic_NP_Uniprot[NP] + "," + dic_PDB_Pfam[pdb_chain]
        print(out_line)
    except:
        pass
        # print(line)
    
