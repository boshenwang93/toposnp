#! /usr/bin/python3
import re 

## From raw dbSNP missense entry, Select entries with Mappable NP
def Fetch_dbSNP_w_NP(dbSNP_missense_file, NP_list_file, mappable_NP_file):

    ## initialize list for mappable rsSNP
    list_mappable_rsSNP = []

    ## obtain the NP list
    list_mappable_NP = []
    f_np = open(NP_list_file, 'r')
    for line in f_np:
        line = line.strip()
        if line != '':
            list_mappable_NP.append(line)
    f_np.close()
    
    ## Read the raw dbSNP missense entry
    out = open(mappable_NP_file, 'w')

    f_dbSNP = open(dbSNP_missense_file, 'r')
    for line in f_dbSNP:
        list_slices = re.split(",", line)
        rsSNP_id = list_slices[0].strip()
        chromosome_location = list_slices[5].strip()

        for i in range(6, len(list_slices) - 1):
            tmp_prot_whole_entry = list_slices[i]
            if re.search("prot_acc", tmp_prot_whole_entry):
                NP_id = re.split("=", tmp_prot_whole_entry)[-1]
                
                if NP_id in list_mappable_NP:

                    out_line = rsSNP_id + "," +\
                               chromosome_location + "," +\
                               tmp_prot_whole_entry + "\n"
                    out.write(out_line)
            else:
                pass
    f_dbSNP.close()

    out.close()
    return 0 


Fetch_dbSNP_w_NP( dbSNP_missense_file = '/home/bwang/project/toposnp/script/dbSNP/Only_dbSNP_missense',
                   NP_list_file = '/home/bwang/project/toposnp/script/CrossReference/NP_list',
                   mappable_NP_file = 'mappable_rsSNP' )