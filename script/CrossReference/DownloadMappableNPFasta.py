#! /usr/bin/python3

import re
import urllib3

###########################################################
#### API Key: 3b2244872b9561678f73f4a6240cbdcf0908 ########
#### Must remember: 10 Requests per second ################
###########################################################

def DownloadNcbiFasta(NCBI_ID, Assigned_download_folder):

    # assemble the efetch URL
    URL_base = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/"

    # epost for later processing
    URL_epost = URL_base + "epost.fcgi?db=protein&id=" + NCBI_ID

    # efetch for retreive the fasta file
    URL_efetch = URL_base + "efetch.fcgi?db=protein&id=" + NCBI_ID +\
        "&query_key=$key1&WebEnv=$web1" +\
        "&rettype=fasta" +\
        "&retmote=text" +\
        "&api_key=3b2244872b9561678f73f4a6240cbdcf0908"

    # initalize the connection of http
    http = urllib3.PoolManager()

    r = http.request('GET',
                     URL_efetch,
                     timeout=1.0,
                     retries=3,
                     preload_content=False)

    # new fasta file path
    fasta_file_path = Assigned_download_folder + NCBI_ID + '.fasta'
    with open(fasta_file_path, 'wb') as out:
        while True:
            data = r.read()
            if not data:
                break
            out.write(data)

    r.release_conn()

    return 0

def main():

    ## initialize to get mappable NP name list 
    list_mappable_NP = [] 

    f = open('/home/bwang/project/toposnp/script/MysqlDBcsv/NP2rsSNP.csv', 'r')
    for line in f:
        NP_name = re.split(",", line)[0].strip()
        if NP_name not in list_mappable_NP:
            list_mappable_NP.append(NP_name)
            try:
                DownloadNcbiFasta(NCBI_ID = NP_name, 
                              Assigned_download_folder = '/home/bwang/project/toposnp/data/fasta/')
            except:
                print(NP_name)
    f.close()

    return 0


if __name__ == "__main__":
    main()