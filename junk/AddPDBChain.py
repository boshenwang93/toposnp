#! /usr/bin/python3

import re 

def ObtainHashNP2PDB(NP2PDB_csv_file):

    ## initialize the unique NP list || Dictionar NP => list PDB
    list_unique_NP = []
    dic_NP_PDBlist = {}

    f = open(NP2PDB_csv_file, 'r')
    for line in f:
        line = line.strip()
        NP_id = re.split(",", line)[1]
        if NP_id not in list_unique_NP:
            list_unique_NP.append(NP_id)
    f.close()

    for every_NP in list_unique_NP:
        list_corresponding_PDB = []

        f = open(NP2PDB_csv_file, 'r')
        for line in f:
            line = line.strip()
            NP_id = re.split(",", line)[1]
            PDB_Chain = re.split(",", line)[2]

            if every_NP == NP_id:
                list_corresponding_PDB.append(PDB_Chain)
        f.close()
        
        dic_NP_PDBlist[every_NP] = list_corresponding_PDB
    
    return dic_NP_PDBlist


def main():
    dictionary_NP_PDBlist = ObtainHashNP2PDB(NP2PDB_csv_file = '/home/bwang/project/toposnp/script/MysqlDBcsv/NP2PDB.csv' )
    
    f = open('mappable_rsSNP2NP.csv', 'r')
    for line in f:
        line = line.strip()
        NP = re.split(",", line)[1]
        
        try:
            list_PDB_chain = dictionary_NP_PDBlist[NP]
            for each_pdb_chain in list_PDB_chain:
                out_line = line + "," + each_pdb_chain
                print(out_line)
        except:
            pass
    f.close()

    return 0

if __name__ == "__main__":
    main()