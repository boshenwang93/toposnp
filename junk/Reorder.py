#! /usr/bin/python3 
import re 


f = open('rsSNPWholeTable', 'r')
for line in f:
    line = line.strip()
    line_slices = re.split(",", line)
    rsSNP = line_slices[0]
    NP = line_slices[1]
    NP_index = line_slices[2]
    mutated_AA = line_slices[3]
    pdb_chain = line_slices[4]
    pdb_seqnum = line_slices[5]
    wild_AA = line_slices[6]
    sasa = line_slices[7]
    location = line_slices[8]
    
    PDB_NP = pdb_chain + "_" + NP

    out_line = PDB_NP + "," + pdb_seqnum + "," +\
               NP_index + "," + rsSNP + "," +\
               wild_AA + "," + mutated_AA + "," +\
               sasa + "," + location 
    print(out_line)

f.close()