#! /usr/bin/python3

import re 

def ObtainHashNPPDB2Seqnum(whole_residue_mapping_file):

    ## initialize the Dictionar NP_Index_PDB => PDB Seqnum
    dic_NPindexPDB_Seqnum = {}
    dic_NPindexPDB_wildAA = {}

    f = open(whole_residue_mapping_file, 'r')
    for line in f:
        line = line.strip()
        NP_id = re.split(",", line)[0]
        PDB_wChain = re.split(",", line)[1]
        wild_residue = re.split(",", line)[2]
        NP_index = re.split(",", line)[3]
        PDB_Seqnum = re.split(",", line)[4]

        ID = NP_id + "," + PDB_wChain + "," + NP_index

        dic_NPindexPDB_Seqnum[ID] = PDB_Seqnum
        dic_NPindexPDB_wildAA[ID] = wild_residue
    f.close()
    
    return dic_NPindexPDB_Seqnum, dic_NPindexPDB_wildAA


def main():
    dictionary_NPindexPDB_Seqnum, dictionary_NPindexPDB_wildAA =\
        ObtainHashNPPDB2Seqnum(whole_residue_mapping_file = '/home/bwang/project/toposnp/script/align/Whole_Residue_Mapping.table' )
    
    f = open('rsSNP2NPPDB', 'r')
    for line in f:
        line = line.strip()
        NP = re.split(",", line)[1]
        NP_index = re.split(",", line)[2]
        PDB_chain = re.split(",", line)[4]

        ID = NP + "," + PDB_chain + "," + NP_index
        
        try:
            wild_aa = dictionary_NPindexPDB_wildAA[ID]
            PDB_seqnum = dictionary_NPindexPDB_Seqnum[ID]

            out_line = line + "," + PDB_seqnum + "," + wild_aa
            print(out_line)
        except:
            pass
    f.close()

    return 0

if __name__ == "__main__":
    main()