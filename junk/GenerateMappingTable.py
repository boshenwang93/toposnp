#! /usr/bin/python3

import re 
import os 


def MappingTable(raw_mapping_file_path):

    ## initialize the list for residue mapping line 
    list_mapping_residue = []
 
    ## file name rule: identity, NP, PDB_Chain
    raw_mapping_file = re.split("/", raw_mapping_file_path)[-1]
    NP_id = re.split("_>", raw_mapping_file)[1].strip()
    PDB_chain = re.split("_>", raw_mapping_file)[2].strip()
    PDB_chain = PDB_chain[0:6]

    f = open(raw_mapping_file_path, 'r')
    for line in f:
        line = line.strip()
        if line != "":
            line_slices = re.split(",", line)
            fasta_index = line_slices[-2]
            pdb_seqnum = line_slices[-1]

            residue_singleLetter = line_slices[-3]
        
            out_entry = NP_id + "," + PDB_chain + "," + residue_singleLetter +\
                    "," + fasta_index + "," + pdb_seqnum
            list_mapping_residue.append(out_entry)
    f.close()

    return list_mapping_residue

MappingTable(raw_mapping_file_path = '/data/database/B151TopoSNP/BestMapping/1.0_>NP_997001.1_>2JNJ_B_start_1_end_74.mapping')

def main(assigned_folder):

    for root, subdirs, files in os.walk(assigned_folder):
        for every_file in files:
            if every_file.endswith("mapping"):
                file_path = root + every_file
                try:
                    list_mapping = MappingTable(raw_mapping_file_path = file_path)
                    for each_residue in list_mapping:
                        print(each_residue)
                except:
                    pass
                    ## Weird 2JNJ_B_start_1_end_74.mapping
    return 0 

if __name__ == "__main__":
    main(assigned_folder = '/data/database/B151TopoSNP/BestMapping/' )