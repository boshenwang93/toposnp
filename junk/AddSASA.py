#! /usr/bin/python3

import re
import os 

def ObtainDictionarySASA(SASA_out_dir):
    
    ## initialize the dictionary PDB+seqnum => SASA/Geometric 
    dic_PDBResidue_SASA = {}

    for root, subdirs, files in os.walk(SASA_out_dir):
        for everyfile in files:
            if everyfile.endswith("SASA"):
                file_path = root + everyfile 
                f = open(file_path, 'r')
                for line in f:
                    line = line.strip()
                    line_slices = re.split(",", line)
                    pdb = line_slices[0]
                    chain = line_slices[1]
                    seqnum = line_slices[2]
                    residue_tri = line_slices[3]
                    sasa = line_slices[4]
                    location = line_slices[5]

                    pdb_chain_seqnum = pdb + "_" + chain + "," + seqnum 
                    sasa_info = sasa + "," + location

                    dic_PDBResidue_SASA[pdb_chain_seqnum] = sasa_info

                f.close()

    return dic_PDBResidue_SASA


def main():
    
    dic_PDBseqnum_SASA = ObtainDictionarySASA(SASA_out_dir = '/data/database/B151TopoSNP/pdb/single_conform/')

    f = open('rsSNPNPPDBmapping', 'r')
    for line in f:
        line = line.strip()
        
        PDB_chain = re.split(",", line)[4]
        seqnum =  re.split(",", line)[5]

        ID = PDB_chain + "," + seqnum
        
        try:
            sasa_info = dic_PDBseqnum_SASA[ID]
            out_line = line + "," + sasa_info
            print(out_line)
        except:
            pass
    f.close()

    return 0

if __name__ == "__main__":
    main()