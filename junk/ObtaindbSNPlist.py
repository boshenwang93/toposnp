#! /usr/bin/python3
import re

# Parse dbSNP missense info
def ParseMissenseInfo(dbsnp_missense_file_path):
    
    ## Get the mappable NP 
    list_mappable_NP = []
    f = open('/home/bwang/project/toposnp/script/MysqlDBcsv/NP2rsSNP.csv', 'r')
    for line in f:
        NP_name = re.split(",", line)[0].strip()
        if NP_name not in list_mappable_NP:
            list_mappable_NP.append(NP_name)
    f.close()

    # Open the file handle
    f = open(dbsnp_missense_file_path, 'r')
    for line in f:
        slices_list = re.split(',', line)
        rsSNP = slices_list[0].strip()

        for element in slices_list:
            element = element.strip()
            if re.search("prot", element):
                prot_whole = re.split("\|", element)[-1]
                prot_acc = re.split("=", prot_whole)[-1]
                prot_acc = prot_acc.strip()

                aa_position_whole = re.split("\|", element)[-3]
                aa_position = re.split("=", aa_position_whole)[1]

                mutated_residue_info = re.split("\|", element)[1]
                mutated_residue = re.split("=", mutated_residue_info)[1]

                out_line = rsSNP + "," + prot_acc +\
                           "," + aa_position + "," +\
                           mutated_residue
                if prot_acc.startswith("NP"):
                    print(out_line)
    f.close()

    return 0

ParseMissenseInfo(dbsnp_missense_file_path = '/data/Dropbox/project/toposnp/B151/Only_dbSNP_missense')
