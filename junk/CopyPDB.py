#! /usr/bin/python3

import re 
import os 
import sys

for root, subdirs, files in os.walk('/data/database/B151TopoSNP/pdb/single_conform/'):
    for every_file in files:
        if every_file.endswith('pdb'):
            if every_file.endswith('poc.pdb') == 0 :
                original_file_path = root + every_file
                new_destination = '/data/database/B151TopoSNP/pdb/pdbweb/'
                cmd_copy = 'cp ' + original_file_path + "\t" + new_destination
                os.system(cmd_copy)
            