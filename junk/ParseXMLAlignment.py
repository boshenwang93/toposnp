#! /usr/bin/python3

import xml.etree.ElementTree as ET
import os
import re

#######################################
#### Parse BLAST XML Out ##############
#######################################

def ChooseHits(BLAST_XML_Output_file):

    # initialize dictionary(uniprot=> list hits)
    list_hits = []
    uniprot_id = ''
    reference_length = ''
    # dictionary for candidate hit => match length
    dic_hit_MatchingCoverage  = {}

    # read the XML file into a ET parser
    tree = ET.parse(BLAST_XML_Output_file)
    root = tree.getroot()

    # find hits
    for first_layer in root:

        ## capture reference sequence ID 
        if 'BlastOutput_query-def' == first_layer.tag:
            query_info = first_layer.text
            uniprot_id += re.split("\s", query_info)[0]
            uniprot_id = uniprot_id.strip().upper()

        ## capture the total length of reference sequence 
        
        elif 'BlastOutput_query-len' == first_layer.tag:
            reference_length += first_layer.text
            reference_length = int(reference_length)

        # find the iterations segments
        elif 'BlastOutput_iterations' == first_layer.tag:
            for second_layer in first_layer:
                if 'Iteration' == second_layer.tag:
                    for third_layer in second_layer:
                        if 'Iteration_hits' == third_layer.tag:
                            for fourth_layer in third_layer:
                                if 'Hit' == fourth_layer.tag:
                                    pdb_chain = ''
                                    hit_begin = ''
                                    hit_end = ''
                                    for fifth_layer in fourth_layer:
                                        if 'Hit_def' == fifth_layer.tag:
                                            pdb_chain += fifth_layer.text

                                        if 'Hit_hsps' == fifth_layer.tag:

                                            for sixth_layer in fifth_layer:
                                                if 'Hsp' == sixth_layer.tag:
                                                    for seventh_layer in sixth_layer:
                                                        if 'Hsp_hit-from' == seventh_layer.tag:
                                                            hit_begin += seventh_layer.text
                                                        if 'Hsp_hit-to' == seventh_layer.tag:
                                                            hit_end += seventh_layer.text

                                    list_hits.append(pdb_chain)
                                    match_length = int(hit_end) - int(hit_begin)

                                    dic_hit_MatchingCoverage[pdb_chain] = match_length / reference_length

    return uniprot_id, list_hits, dic_hit_MatchingCoverage


def main():

    # dic for uniprot => list best hits
    dic_uniprot_besthits = {}

    # BLAST XML output path
    BLAST_XML_dir = '/home/bwang/project/toposnp/data/blast/out_xml/'

    # uniprot fasta file path
    uniprot_fasta_dir = '/home/bwang/project/toposnp/data/NCBI/fasta/'
    
    # BLAST all fasta in folder
    for root, dirs, files in os.walk(BLAST_XML_dir):
        for single_file in files:
            file_asb_path = BLAST_XML_dir + single_file

            if single_file.endswith('.XML'):
                uniprot, list_hits, dic_hit_length = ChooseHits(
                    BLAST_XML_Output_file=file_asb_path)

                
                out_line = uniprot  + "|"
                default_out_line = uniprot  + "|"
                if list_hits != []: 
                    for each_hit in list_hits:
                        if dic_hit_length[each_hit] >= 0.8: 
                            out_line += each_hit + "|"
                
                if out_line != uniprot  + "|":
                    print(out_line)
    return 0


if __name__ == '__main__':
    main()
