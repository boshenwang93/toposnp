#! /usr/bin/python3
import re 
import os 
 
## Select the best identical candidate
## copy to new folder 
def SelectBestIdentityCandidate(raw_mapping_folder, new_best_iden_mapping_folder):
 
    ## initialize the dictionary for NP => Best identity 
    dic_NP_BestIden = {}
    list_unique_NP = [] 
 
    ## visit the folder contains all candidate mapping
    ## file name rule: identity, NP, PDB_Chain
 
    ## First iterative to capture list of unique NP ids
    for root, subdirs, files in os.walk(raw_mapping_folder):
        for each_file in files:
            if each_file.endswith("mapping"):
                try:
                    NP_id = re.split("_>", each_file)[1]
                    if NP_id not in list_unique_NP:
                        list_unique_NP.append(NP_id)
                except:
                    pass
    
    ## Second iterative to capture dictionary NP => Best identity
    for i in range(0, len(list_unique_NP)):
    # for i in range(0, 20):
        each_NP = list_unique_NP[i]
        list_all_identity = []
 
        for root, subdirs, files in os.walk(raw_mapping_folder):
            for each_file in files:
                if each_file.endswith("mapping"):
                    try:
                        NP_id = re.split("_>", each_file)[1]
                        identity_string = re.split("_>", each_file)[0]
                        if NP_id == each_NP:
                            identity_float = float(identity_string)
                            list_all_identity.append(identity_float)
                    except:
                        pass
        max_identity = max(list_all_identity)
        dic_NP_BestIden[each_NP] = max_identity
 
    ## Copy the best identical candidate to new folder
    for k,v in dic_NP_BestIden.items():
        file_prefix = str(v) + "_>" + str(k)
        for root, subdirs, files in os.walk(raw_mapping_folder):
            for each_file in files:
                if each_file.startswith(file_prefix):
                    each_file = re.sub(">", "\>", each_file)
                    file_abs_path = root + each_file
                    copy_command = "cp " + file_abs_path + " " + new_best_iden_mapping_folder
                    # print(copy_command)
                    os.system(copy_command)
    return 0
 
 
def main(in_dir, out_dir):
    SelectBestIdentityCandidate (raw_mapping_folder = in_dir,
                                 new_best_iden_mapping_folder = out_dir )
    return 0 
 
if __name__ == "__main__":
    main(in_dir = '/data/database/B151TopoSNP/mapping/',
         out_dir = '/data/database/B151TopoSNP/BestMapping/')