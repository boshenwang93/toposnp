#! /usr/bin/python3

import re
import os 
from multiprocessing import Process, Pool


#### Calculate identity and Residue-level mapping 
def ReadIdentity(alignedFasta_file):
    threshold_iden = 0.7
    uniprot_header =''
    pdb_header = ''

    with open(alignedFasta_file) as f:
        all_context = f.read()
    f.close()

    list_slices = re.split('>', all_context)

    seq1_w_header = '>' + list_slices[1]
    seq2_w_header = '>' + list_slices[2]

    list_lines_seq1 = re.split("\n", seq1_w_header)
    list_lines_seq2 = re.split("\n", seq2_w_header)
    
    seq1_context = ''
    seq2_context = ''

    for eachline in list_lines_seq1:
        if eachline.startswith('>') == 0 :
            if eachline != '':
                seq1_context += eachline
        else:
            uniprot_header += eachline
                
    for eachline in list_lines_seq2:
        if eachline.startswith('>') == 0 :
            if eachline != '':
                seq2_context += eachline
        else:
            pdb_header += eachline

    ##obtain pdb start seqnum 
    pdb_header_slices = re.split('_', pdb_header)
    begin_seq_num_pdb = int( pdb_header_slices[3])
    pdb_chain_id = pdb_header_slices[0] + ':' + pdb_header_slices[1] +',' +\
                   'start:' + pdb_header_slices[3] + ',' +\
                   'end:' + pdb_header_slices[5]
    pdb_chain_id = re.sub('>', '', pdb_chain_id)

    ## get uniprot
    list_uniprot_header = re.split("\s", uniprot_header)
    uniprot_id = list_uniprot_header[0].strip()

    ## check length equal 
    m = len(seq1_context)
    n = len(seq1_context)
    
    ## count identity
    gaps_seq1 = 0 
    gaps_seq2 = 0 
    identity_chars = 0 
    if m == n :
        for i in range(0, m ,1):
            if seq1_context[i] == seq2_context[i]:
                identity_chars += 1
            if seq1_context[i] == '-':
                gaps_seq1 += 1
            if seq2_context[i] == '-':
                gaps_seq2 += 1

    seq1_wo_gap = re.sub('-', '', seq1_context)
    seq_identity = identity_chars / len(seq1_wo_gap)
    
    
    ## Match info 
    matching_entry = ''
    for i in range(0, m, 1):
        ## for seq 1 
        ## count gaps before i+1 th string
        Seq1_gap_before = 0
        for j in range(0, i, 1):
            if seq1_context[j] == '-':
                Seq1_gap_before += 1 
        actual_index_uniprot = i - Seq1_gap_before + 1

        Seq2_gap_before = 0
        for j in range(0, i, 1):
            if seq2_context[j] == '-':
                Seq2_gap_before += 1 
        index_pdb = i - Seq2_gap_before + 1
        actual_index_pdb = index_pdb - 1 + begin_seq_num_pdb

        if seq1_context[i] != '-':
            if seq2_context[i] != '-':
                if seq1_context[i] == seq2_context[i]:
                    out_entry = uniprot_id + ',' +\
                                pdb_chain_id + ',' +\
                                str(seq_identity) + ',' +\
                                seq1_context[i] + ',' +\
                                str(actual_index_uniprot) + ',' +\
                                str(actual_index_pdb) + "\n"
                    matching_entry += out_entry
    

    if seq_identity > threshold_iden:
        out_file_path = '/home/bwang/project/toposnp/data/alignment/mapping/' +\
                        str(seq_identity) + "_" + uniprot_header +\
                        "_" + pdb_header + ".mapping"
        out = open(out_file_path, 'w')
        out.write(matching_entry)
        out.close()
    
    seq_identity_info = uniprot_id + ',' +\
                        pdb_chain_id + ',' + str(seq_identity)
    
    out_test =  '/home/bwang/project/toposnp/data/alignment/test/' + uniprot_header +\
                        "_" + pdb_header
    test = open(out_test, 'w')
    test.write("1")
    test.close()
    return seq_identity_info, seq_identity
    

def main(aligned_fasta_folder):
    # iterating the pdb folder
    list_aligned_file = []

    for root, subdirs, files in os.walk(aligned_fasta_folder):
        for file in files:
            if file.endswith("fasta"):
                file_path = root + file
                list_aligned_file.append(file_path)

    ## Multiple processing
    with Pool(processes= 10) as pool:
        print(pool.map(ReadIdentity, list_aligned_file))

    return 0

if __name__ == "__main__":
    main(aligned_fasta_folder = '/home/bwang/project/toposnp/data/alignment/clustal_out/')

