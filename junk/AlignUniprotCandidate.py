#! /usr/bin/python3

import re 
import os 

## Retrieve the sequence in fasta format With header 
def Retrieve_Uniprot_Seq(uniprot_fasta_file_path):
    seq_content = ''
    with open(uniprot_fasta_file_path, 'r') as f:
        seq_content += f.read()
    f.close()
    return seq_content

def Retrieve_PDBfasta_Seq(PDB_fasta_file_path):
    seq_content = ''
    with open(PDB_fasta_file_path, 'r') as f:
        seq_content += f.read()
    f.close()
    return seq_content

#### read Candidate file 
def ReadCandidate(candiate_file):
    ## dictionary uniport => list candidate/ besthits
    dic_uniport_candiate = {}

    f = open(candiate_file, 'r')
    for line in f:
        if re.search("\|", line):
            slices = re.split("\|", line)
            uniprot = slices[0].strip()
            list_NR_hits = []
            for i in range(1, len(slices)):
                pdb_chain = slices[i].strip()
                if pdb_chain not in list_NR_hits:
                    if pdb_chain != '':
                        list_NR_hits.append(pdb_chain)
            
            dic_uniport_candiate[uniprot] = list_NR_hits
    f.close()

    return dic_uniport_candiate


## Give uniprot, pdb, generate New pairwise fasta file 
def GeneratePairWiseFile(uniprot_id, pdb_chain, 
                         uniprot_fasta_dir, pdb_dir,
                         out_file_dir):
    ## Get uniprot sequence 
    uniprot_id = uniprot_id.upper()
    uniprot_fasta_abs_path = uniprot_fasta_dir + uniprot_id + '.fasta'
    uniprot_seq = Retrieve_Uniprot_Seq(uniprot_fasta_file_path = uniprot_fasta_abs_path)

    ## Get pdb chain sequence 
    pdb_chain_fasta_abs_path = pdb_dir + pdb_chain + '.fasta'
    pdb_seq = Retrieve_PDBfasta_Seq(PDB_fasta_file_path = pdb_chain_fasta_abs_path)
    
    ## new pairwise file
    out_file_name = uniprot_id + '_' + pdb_chain
    out_path = out_file_dir + out_file_name
    out = open(out_path, 'w')
    out.write(uniprot_seq)
    out.write("\n")
    out.write(pdb_seq)
    out.close()
    return 0

#### Run clustalW for pairwise align  
def RunClustw(input_fasta, output_fasta):
    cmd = 'clustalw2 -TYPE=PROTEIN -OUTPUT=FASTA -PWMATRIX=BLOSUM ' +\
          ' -INFILE=' + input_fasta +\
          ' -OUTFILE=' + output_fasta 
    os.system(cmd)

    return 0 

def main(candidate_file):
    ## get dictionary uniprot => candidate
    dic_uniprot_candidate = ReadCandidate(candidate_file)

    ## iterate all pairs
    for k,v in dic_uniprot_candidate.items():
        for each_hit in v:
            each_hit = re.sub(":", "_", each_hit)
            each_hit = re.sub(",", "_", each_hit)

            ## generate pairwise fasta file 
            GeneratePairWiseFile(uniprot_id = k , pdb_chain = each_hit,
                                 uniprot_fasta_dir = '/home/bwang/project/toposnp/data/NCBI/fasta/',
                                 pdb_dir = '/home/bwang/project/toposnp/data/PDBfasta/',
                                 out_file_dir = '/home/bwang/project/toposnp/data/alignment/candidate_pairwise/')
            
            ## Run the clustal
            input_file_name = k + '_' + each_hit
            out_file_name = k + '_' + each_hit + '.fasta'
            pairwise_fasta_dir = '/home/bwang/project/toposnp/data/alignment/candidate_pairwise/'
            out_aligned_dir = '/home/bwang/project/toposnp/data/alignment/clustal_out/'
            input_for_clustal = pairwise_fasta_dir + input_file_name
            output_for_clustal = out_aligned_dir + out_file_name
            RunClustw (input_fasta = input_for_clustal,
                        output_fasta = output_for_clustal)
    return 0 

if __name__ == "__main__":
    main(candidate_file = '/home/bwang/project/toposnp/script/align/NP_PDBcandidate')

